FROM debian:bullseye-slim

ENV DEBIAN_FRONTEND=noninteractive
RUN apt update -q
RUN apt install -y android-tools-mkbootimg wget unzip
RUN wget https://dl.google.com/android/repository/platform-tools-latest-linux.zip
RUN unzip platform-tools-latest-linux.zip -d /android-platform-tools
ENV PATH="$PATH:/android-platform-tools/platform-tools/"
RUN rm -rf /var/lib/apt/lists/*
